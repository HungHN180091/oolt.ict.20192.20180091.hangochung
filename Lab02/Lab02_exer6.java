import java.util.Scanner;

public class Lab02_exer6 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter number of elements: ");
        int numberOfElements = in.nextInt();
        
        double[] doubleArr = new double[numberOfElements];
        System.out.print("Enter the elements: ");
        for (int i=0;i<doubleArr.length;i++)
            doubleArr[i] = in.nextDouble();
        in.close();

        for (int i=0;i<doubleArr.length-1;i++)
            for (int j=i+1;j<doubleArr.length;j++)
            {
                if (doubleArr[i] > doubleArr[j])
                {
                    double temp = doubleArr[i];
                    doubleArr[i] = doubleArr[j];
                    doubleArr[j] = temp;
                }
            }
        
        int sum = 0;
        for (int i=0;i<doubleArr.length;i++)
        {
            sum += doubleArr[i];
        }

        System.out.println("The elements of array after sorting:");
        for (int i=0; i<doubleArr.length; i++)
        {
            System.out.print(doubleArr[i] + " ");
        }
        System.out.println("\nThe sum of all elements is " + sum);
        System.out.println("\nThe average of all elements is " + sum/doubleArr.length);
    }
}