import java.util.Scanner;

public class Lab02_exer4 {
    public static void main(String[] args)
    {
        int n;
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        in.close();
        for (int row=0;row<n;row++)
        {
            for (int space=0;space < n-1-row;space++)
                System.out.print(" ");
            for (int star=0;star<2*row+1;star++)
                System.out.print("*");
            for (int space=0;space < n-1-row;space++)
                System.out.print(" ");
            System.out.print("\n");
        }
    }
}