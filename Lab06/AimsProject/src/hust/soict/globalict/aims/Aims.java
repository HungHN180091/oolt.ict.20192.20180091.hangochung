package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	public static void main(String[] args) {
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95F);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95F);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99F);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		ArrayList<Media> AvailableItems = new ArrayList<Media>();
		AvailableItems.add(dvd1);
		AvailableItems.add(dvd2);
		AvailableItems.add(dvd3);
		
		int opt;
		Scanner in = new Scanner(System.in);
		Order order = null;
		
		while (true)
		{
			showMenu();
			System.out.println("Enter your choice: ");
			opt = in.nextInt();
			switch (opt)
			{
				case 1:
				{
					order = new Order();
					System.out.println("New order is created successfully.");
					break;
				}
				
				case 2:
				{
					if (order != null)
					{
						System.out.println("All available items: ");
						for (Media m: AvailableItems)
						{
							System.out.println(AvailableItems.indexOf(m) + ".\tTitle: " + m.getTitle()
												+ "\tCategory: " + m.getCategory() + "\tCost: " + m.getCost());
						}
						
						int item;
						do {
							System.out.print("Select the item you want to add: ");
							item = in.nextInt();
						} while (item < 0 || item > AvailableItems.size());
						order.addMedia(AvailableItems.get(item));
						break;
					}
					else
					{
						System.out.println("You must create a new order first.");
						break;
					}
				}
				
				case 3:
				{
					if (order != null)
					{
						System.out.println("Items in the order:");
						order.displayItems();
						int item;
						do {
							System.out.print("Enter the ID item you want to remove: ");
							item = in.nextInt();
						} while (!order.removeMedia(item));
						break;
					}
					else
					{
						System.out.println("You haven't created an order yet.");
						break;
					}
				}
				
				case 4:
				{
					if (order != null)
					{
						System.out.println("Items in the order:");
						order.displayItems();
						break;
					}
					else
					{
						System.out.println("You haven't created an order yet.");
						break;
					}
				}
				
				case 0:
				{
					in.close();
					return;
				}
				
				default:
				{
					System.out.println("Invalid option.");
					break;
				}
			}
		}
		
	}
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
}
