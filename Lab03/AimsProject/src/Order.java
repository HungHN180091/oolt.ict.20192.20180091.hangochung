
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	
	private int qtyOrdered;

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if (qtyOrdered < MAX_NUMBER_ORDERED) {
			this.itemsOrdered[qtyOrdered++] = disc;
			System.out.println("Disc has been added!");
		}
		else System.out.println("The order queue is full!");
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		for (int i=0;i < qtyOrdered;i++)
		{
			if (itemsOrdered[i] == disc)
			{
				for (int j=i;j< qtyOrdered-1; j++ )
					itemsOrdered[j] = itemsOrdered[j+1];
				itemsOrdered[qtyOrdered-1] = null;
				qtyOrdered--;
				System.out.println("Disc #" + (i+1) + " has been removed successfully!");
				return;
			}
		}
	}
	
	public float totalCost()
	{
		float cost = 0;
		for (int i=0; i<qtyOrdered; i++)
		{
			cost += itemsOrdered[i].getCost();
		}
		return  cost;
	}

}
