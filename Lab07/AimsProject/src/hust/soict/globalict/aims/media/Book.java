package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	
	private List<String> authors = new ArrayList<String>();

	public Book(String title) {
		super(title);
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public void addAuthor(String name)
	{
		for (String author: authors)
		{
			if (author == name)
			{
				System.out.println("The author is already in the list.");
				return;
			}
		}
		
		authors.add(name);
		System.out.println("The author is added successfully.");
	}
	
	public void removeAuthor(String name)
	{
		for (String author: authors)
		{
			if (author == name)
			{
				authors.remove(name);
				System.out.println("Author remove successfully.");
				return;
			}
		}
		System.out.println("There is no matching author.");		
	}
}
