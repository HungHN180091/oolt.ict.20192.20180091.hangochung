package hust.soict.globalict.aims.media.disc;

import java.util.ArrayList;

import hust.soict.globalict.aims.media.Playable;

public class CompactDisc extends Disc implements Playable {

	private String artist;
	private ArrayList<Track> tracks;
	
	public CompactDisc(String title, String category, String director, float cost) {
		super(title, category, director, 0, cost);
		this.tracks = new ArrayList<Track>();
	}

	public CompactDisc(String title) {
		super(title);
		this.tracks = new ArrayList<Track>();
	}

	public CompactDisc(String title, String category) {
		super(title, category);
		this.tracks = new ArrayList<Track>();
	}

	public CompactDisc(String title, String category, String director) {
		super(title, category, director);
		this.tracks = new ArrayList<Track>();
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track t)
	{
		if (tracks.contains(t))
		{
			System.out.println("This track is already in the CD");
			return;
		}
		else
		{
			tracks.add(t);
			System.out.println("The track is added successfully");
			return;
		}
	}
	
	public void removeTrack(Track t)
	{
		if (!tracks.contains(t))
		{
			System.out.println("This track is not in the CD");
			return;
		}
		else
		{
			tracks.remove(t);
			System.out.println("The track is removed successfully");
			return;
		}
	}
	
	public int getLength() {
		int total = 0;
		for (Track t: tracks)
		{
			total += t.getLength();
		}
		return total;
	}

	@Override
	public void play() {
		for (Track t: tracks)
		{
			t.play();
		}		
	}
}
