package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.media.disc.*;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	public static void main(String[] args) {		
		/*
		int opt;
		Scanner in = new Scanner(System.in);
		Order order = null;
		
		while (true)
		{
			showMenu();
			System.out.println("Enter your choice: ");
			opt = in.nextInt();
			switch (opt)
			{
				case 1:
				{
					order = new Order();
					System.out.println("New order is created successfully.");
					break;
				}
				
				case 2:
				{
					if (order != null)
					{
						int choice;
						do {
							System.out.println("Enter the type of items: 1. DVD\t 2. CD\t 3. Book");
							choice = in.nextInt();
						} while (choice > 3 || choice < 1);
						
						switch (choice)
						{
							case 1:
							{
								System.out.println("Enter the title: ");
								String title = in.next();
								System.out.println("Enter the category: ");
								String category = in.next();
								System.out.println("Enter the director: ");
								String director = in.next();
								System.out.println("Enter the length: ");
								int length = in.nextInt();
								System.out.println("Enter the cost: ");
								float cost = in.nextFloat();
								order.addMedia(new DigitalVideoDisc(title, category, director, length, cost));
								break;
							}
							
							case 2:
							{
								System.out.println("Enter the title: ");
								String title = in.next();
								System.out.println("Enter the category: ");
								String category = in.next();
								System.out.println("Enter the director: ");
								String director = in.next();
								System.out.println("Enter the artist: ");
								String artist = in.next();
								System.out.println("Enter the cost: ");
								float cost = in.nextFloat();
								CompactDisc currentDisc  = new CompactDisc(title, category, director, cost);
								currentDisc.setArtist(artist);
								System.out.println("Enter the number of tracks: ");
								int trackNum = in.nextInt();
								for (int i = 0; i < trackNum; i++)
								{
									System.out.println("Enter the track #" + (i+1) + " title: ");
									String trackTitle = in.next();
									System.out.println("Enter the track #" + (i+1) + " length: ");
									int trackLength = in.nextInt();
									currentDisc.addTrack(new Track(trackTitle, trackLength));
								}
								order.addMedia(currentDisc);
								break;
							}
							
							case 3:
							{
								System.out.println("Enter the title: ");
								String title = in.next();
								System.out.println("Enter the category: ");
								String category = in.next();
								System.out.println("Enter the cost: ");
								float cost = in.nextFloat();
								Book currentBook = new Book(title, category, cost);
								System.out.println("Enter the number of authors: ");
								int authorNum = in.nextInt();
								for (int i = 0; i < authorNum; i++)
								{
									System.out.println("Enter the author #" + (i+1) + " 's name: ");
									String author = in.next();
									currentBook.addAuthor(author);
								}
								order.addMedia(currentBook);
								break;
							}
						}
						break;
					
					}
					else
					{
						System.out.println("You must create a new order first.");
						break;
					}
				}
				
				case 3:
				{
					if (order != null)
					{
						System.out.println("Items in the order:");
						order.displayItems();
						int item;
						do {
							System.out.print("Enter the ID item you want to remove: ");
							item = in.nextInt();
						} while (!order.removeMedia(item));
						break;
					}
					else
					{
						System.out.println("You haven't created an order yet.");
						break;
					}
				}
				
				case 4:
				{
					if (order != null)
					{
						System.out.println("Items in the order:");
						order.displayItems();
						break;
					}
					else
					{
						System.out.println("You haven't created an order yet.");
						break;
					}
				}
				
				case 0:
				{
					in.close();
					return;
				}
				
				default:
				{
					System.out.println("Invalid option.");
					break;
				}
			}
		}*/
		
		MemoryDaemon instanceMD = new MemoryDaemon();
		Thread currentThread = new Thread(instanceMD);
		currentThread.setDaemon(true);
		
	}
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
		}
}
