package hust.soict.globalict.garbage;


public class GarbageCreator {
	private String data;
	private static final int  KEY = 3;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public GarbageCreator(String data) {
		super();
		this.data = data;
	}
	
	public String encode()
	{
		String hash = "";
		for (int i = 0; i < data.length(); i++)
		{
			hash += (char) (data.charAt(i) + KEY);				//generate garbage
		}
		return hash;
	}
	
}
