import java.util.Scanner;

class Lab02_exer7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the size of matrix: ");
        int row = in.nextInt();
        int col = in.nextInt();

        double[][] matrix1 = new double[row][col];
        double[][] matrix2 = new double[row][col];

        System.out.println("Enter the matrix 1:");
        for (int i=0;i<row;i++)
        {
            System.out.print("Enter the row " + (i+1) + ":");
            for (int j=0;j<col;j++)
                matrix1[i][j] = in.nextDouble();
        }

        System.out.println("Enter the matrix 2:");
        for (int i=0;i<row;i++)
        {
            System.out.print("Enter the row " + (i+1) + ":");
            for (int j=0;j<col;j++)
                matrix2[i][j] = in.nextDouble();
        }
        in.close();
        
        Double[][] sumMatrix = new Double[row][col];
        for (int i=0;i<row;i++)
            for (int j=0;j<col;j++)
                sumMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
        
        System.out.println("The sum matrix is ");
        for (int i=0;i<row;i++) {
            for (int j=0;j<col;j++)
                System.out.print(sumMatrix[i][j] + " ");
            System.out.println();
        }
    }
}