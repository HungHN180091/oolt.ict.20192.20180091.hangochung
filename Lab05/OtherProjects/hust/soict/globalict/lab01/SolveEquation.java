import javax.swing.JOptionPane;

public class SolveEquation {
    public static void main(String[] args) {
        String typeOfEquation;
        typeOfEquation = JOptionPane.showInputDialog(null, "1. First degree linear equation\n" + 
                    "2. Two variables system linear equation\n" + "3. Second degree equation",
                        "Choose type of equation", JOptionPane.INFORMATION_MESSAGE);
        if (Long.parseLong(typeOfEquation) == 1)
        {
            String strCofA, strCofB;
            strCofA = JOptionPane.showInputDialog(null, "ax + b = 0\nEnter the a coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofB = JOptionPane.showInputDialog(null, "ax + b = 0\nEnter the b coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            double a = Double.parseDouble(strCofA);
            double b = Double.parseDouble(strCofB);
            JOptionPane.showMessageDialog(null, "The solution of equation " + strCofA + "x + " +
                        strCofB + " = 0 is x = " + (-b/a) );
            System.exit(0);            
        }

        else if (Long.parseLong(typeOfEquation) == 2)
        {
            String strCofA1, strCofB1, strCofA2, strCofB2, strCofC1, strCofC2;
            strCofA1 = JOptionPane.showInputDialog(null, "Enter the a1 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofB1 = JOptionPane.showInputDialog(null, "Enter the b1 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofC1 = JOptionPane.showInputDialog(null, "Enter the c1 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofA2 = JOptionPane.showInputDialog(null, "Enter the a2 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofB2 = JOptionPane.showInputDialog(null, "Enter the b2 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofC2 = JOptionPane.showInputDialog(null, "Enter the c2 coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            double a1 = Double.parseDouble(strCofA1);
            double b1 = Double.parseDouble(strCofB1);
            double c1 = Double.parseDouble(strCofC1);
            double a2 = Double.parseDouble(strCofA2);
            double b2 = Double.parseDouble(strCofB2);
            double c2 = Double.parseDouble(strCofC2);
            double D = a1*b2 - a2*b1;
            double Dx = c1*b2 - c2*b1;
            double Dy = a1*c2 - a2*c1;
            JOptionPane.showMessageDialog(null, "The solution of equation " + strCofA1 + "x + " +
                        strCofB1 + "y = " + strCofC1 + " && " + strCofA2 + "x + " +
                        strCofB2 + "y = " + strCofC2 + "\n is x = " + (Dx/D) + " && y = " +  (Dy/D));
            System.exit(1);
        }
        else {
            String strCofA, strCofB, strCofC;
            strCofA = JOptionPane.showInputDialog(null, "ax^2 + bx + c = 0\nEnter the a coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofB = JOptionPane.showInputDialog(null, "ax^2 + bx + c = 0\nEnter the b coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            strCofC = JOptionPane.showInputDialog(null, "ax^2 + bx + c = 0\nEnter the c coefficient:",
                            "Enter coefficient", JOptionPane.INFORMATION_MESSAGE);
            double a = Double.parseDouble(strCofA);
            double b = Double.parseDouble(strCofB);
            double c = Double.parseDouble(strCofC);
            double Delta = b*b - 4*a*c;
            if (Delta > 0) {
                double x1 = (-b - Math.sqrt(Delta))/(2*a);
                double x2 = (-b + Math.sqrt(Delta))/(2*a);
                JOptionPane.showMessageDialog(null, "The solutions of equation " + strCofA + "x^2 + " +
                        strCofB + "x + " + strCofC + " = 0 is x1 = " + x1 + " and x2 = " + x2);
                System.exit(0);
            }
            
            else if (Delta == 0) {
                double x = -b/(2*a);
                JOptionPane.showMessageDialog(null, "The solution of equation " + strCofA + "x^2 + " +
                        strCofB + "x + " + strCofC + " = 0 is x = " + x);
                System.exit(0);
            }

            else {
                JOptionPane.showMessageDialog(null, "The equation has no solution");
                System.exit(0);
            }
        }

    }
}