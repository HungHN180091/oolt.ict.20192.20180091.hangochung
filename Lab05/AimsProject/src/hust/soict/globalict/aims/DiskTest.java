package hust.soict.globalict.aims;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95F);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95F);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99F);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		Order order = new Order("22:01 26/03/2020");
		DigitalVideoDisc[] list = new DigitalVideoDisc[] {dvd1, dvd2, dvd3};
		order.addDigitalVideoDisc(list);
		
		System.out.println("You got a lucky item: " + order.getALuckyItem().getTitle());
		order.displayOrder();		
	}

}
