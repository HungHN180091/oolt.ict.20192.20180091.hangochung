package hust.soict.globalict.garbage;

public class TestGarbage {

	public static void main(String[] args) {
		GarbageCreator gc = new GarbageCreator("abc");
		NoGarbage ng = new NoGarbage(new StringBuilder("abc"));
		System.out.println(gc.encode());
		System.out.println(ng.encode());

	}

}
