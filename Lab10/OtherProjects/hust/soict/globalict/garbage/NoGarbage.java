package hust.soict.globalict.garbage;

public class NoGarbage {
	private StringBuilder data;
	private static final int  KEY = 3;

	public StringBuilder getData() {
		return data;
	}

	public void setData(StringBuilder data) {
		this.data = data;
	}

	public NoGarbage(StringBuilder data) {
		super();
		this.data = data;
	}
	
	public StringBuilder encode()
	{
		StringBuilder hash = new StringBuilder(); 
		for (int i = 0; i < data.length(); i++)
		{
			hash.append((char) (data.charAt(i) + KEY));				
		}
		return hash;
	}
	
}
