import java.util.Scanner;

public class Lab02_exer5 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the month: ");
        int month = in.nextInt();
        while (month < 0 || month > 12)
        {
            System.out.println("Month must be from 1 to 12!");
            System.out.print("Enter the month: ");
            month = in.nextInt();
        }
        System.out.print("Enter the year: ");
        int year = in.nextInt();
        while (year < 0)
        {
            System.out.println("Year must be positive!");
            System.out.print("Enter the year: ");
            year = in.nextInt();
        }
        in.close();
        switch (month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
            {
                System.out.println("There are 31 days in this month");
                break;
            }
            case 4:
            case 6:
            case 9:
            case 11:
            {
                System.out.println("There are 30 days in this month");
                break;
            }
            case 2:
            {
                if ((year % 100 == 0) & (year % 400 != 0)) {
                    System.out.println("There are 28 days in this month");
                    break;
                }
                else if (year %4 != 0)
                {
                    System.out.println("There are 28 days in this month");
                    break;
                }
                else {
                    System.out.println("There are 29 days in this month");
                    break;
                }
            }
        }
    }
}