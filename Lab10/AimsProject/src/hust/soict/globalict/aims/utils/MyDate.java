package hust.soict.globalict.aims.utils;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private String fullDate;
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if (day>0 && day<31 )
			this.day = day;
		else this.day = 0;
		
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (month > 0 && month < 13)
			this.month = month;
		else this.month = 0;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public void accept()
	{
		Scanner in = new Scanner(System.in);
		do {
		System.out.println("Enter the current date (DD/MM/YYYY): ");
		String date  = in.nextLine();
		String dateSplit[] = date.split("/",0);
	
		setDay(Integer.parseInt(dateSplit[0]));
		if (getDay() == 0)
		{
			System.out.println("Day must be  between 1 and 31!");
			continue;
		}
		
		setMonth(Integer.parseInt(dateSplit[1]));
		if (getMonth() == 0)
		{
			System.out.println("Month must be  between 1 and 12!");
			continue;
		}
		
		setYear(Integer.parseInt(dateSplit[2]));
		} while (getDay() == 0 || getMonth() == 0);
		in.close();
	}
	
	public void	print()
	{
		fullDate = day + "/" + month +"/" + year;
		System.out.print("Current date: " + fullDate);
	}

}
