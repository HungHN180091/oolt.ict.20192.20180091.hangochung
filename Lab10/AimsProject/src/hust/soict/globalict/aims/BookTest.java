package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.List;

import hust.soict.globalict.aims.media.*;

public class BookTest {

	public static void main(String[] args) {
		List<Book> list = new ArrayList<Book>();
		Book book1 = new Book("Inside my Soul", "Science", 14.5f);
		book1.addAuthor("James");
		book1.addAuthor("William Mile");
		book1.setContent("This is a picture,which is a picture inside me.me want me two be silent with with you");
		
		
		Book book2 = new Book("Hanoi in the old eyes", "Romantic", 24.4f);
		book2.addAuthor("Charlie Puzz");
		book2.addAuthor("John Huazi");
		book2.setContent("calculating the following information following the book content "
				+ "This following is , following when the. content of the book is changed.");
		
		
		list.add(book1);
		list.add(book2);
		
		for(Book b: list)
		{
			System.out.println(b.toString());
		}

	}

}
