//Excercise 5: Compute the sum, difference, product and quotient of two double

import javax.swing.JOptionPane;

public class DoubleCalculator {
    public static void main(String[] args)
    {
        String strNum1, strNum2;
        strNum1 = JOptionPane.showInputDialog(null, "Please enter the first number:",
                "Enter the first number", JOptionPane.INFORMATION_MESSAGE);
        strNum2 = JOptionPane.showInputDialog(null, "Please enter the second number:",
                "Enter the second number", JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strNum1);
        double num2 = Double.parseDouble(strNum2);

        String message = "";
        message += strNum1 + "+" + strNum2 + " = " + (num1 + num2) + "\n";
        message += strNum1 + "-" + strNum2 + " = " +  (num1 - num2) + "\n";
        message += strNum1 + "x" + strNum2 + " = " +  (num1 * num2) + "\n";
        if (num2 != 0)
            message += strNum1 + "/" + strNum2 + " = " +  (num1 / num2) + "\n";
        JOptionPane.showMessageDialog(null, message);
        
    }
}