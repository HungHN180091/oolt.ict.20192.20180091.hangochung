package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable<Book>{
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;

	public Book(String title) {
		super(title);
	}

	public Book(String title, String category, float cost) {
		super(title, category, cost);
	}

	public Book(String title, String category) {
		super(title, category);
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public void addAuthor(String name)
	{
		for (String author: authors)
		{
			if (author == name)
			{
				System.out.println("The author is already in the list.");
				return;
			}
		}
		
		authors.add(name);
		System.out.println("The author is added successfully.");
	}
	
	public void removeAuthor(String name)
	{
		for (String author: authors)
		{
			if (author == name)
			{
				authors.remove(name);
				System.out.println("Author remove successfully.");
				return;
			}
		}
		System.out.println("There is no matching author.");		
	}

	@Override
	public int compareTo(Book o) {
		return this.title.compareTo(o.title);
	}
	
	private void processContent()
	{
		String[]tokens = this.content.split(" |,|\\.|\n");
        ArrayList<String> content_tokens = new ArrayList<>();
        Collections.addAll(content_tokens, tokens);
        
        Collections.sort(content_tokens);
        this.contentTokens = content_tokens;
        
        Map<String, Integer> frequency = new HashMap<>();        
        for(String str : content_tokens)
        {
        	int count = 0;
        	for(String str2 : content_tokens)
        	{
        		if(str.equals(str2))
        		{
        			count++;
        		}
        	}
        	frequency.put(str, count);        	
        }
        
        this.wordFrequency = frequency;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public String toString()
	{
		this.processContent();
		String infor = new String();
		infor += ("Category: " + this.getCategory() + "\n");
		infor += ("Cost: " + this.getCost() + "\n");
		infor += "Authors: ";
		for (String author: authors)
		{
			infor += (author);
			if (authors.indexOf(author) != authors.size())
				infor += ", ";
			else infor += "\n";
		}
		infor += ("Number of tokens: " + this.contentTokens.size() + "\n");
		infor += "List of tokens:\n";
		for (String w: this.wordFrequency.keySet())
		{
			infor += (w + ": " + this.wordFrequency.get(w)  +"\n");
		}
		return infor;
	}
}
