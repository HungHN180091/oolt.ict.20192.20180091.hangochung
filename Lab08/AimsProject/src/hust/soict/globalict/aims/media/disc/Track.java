package hust.soict.globalict.aims.media.disc;

import hust.soict.globalict.aims.media.Playable;

public class Track implements Playable, Comparable<Track> {

	private String title;
	private int length;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public Track() {
		// TODO Auto-generated constructor stub
	}
	
	public void play() {
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	@Override
	public int compareTo(Track o) {
		return this.title.compareTo(o.title);
	}

}
