package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.media.disc.*;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	public static void main(String[] args) {		
		ArrayList<Media> collection = new ArrayList<Media>();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95F);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95F);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99F);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		collection.add(dvd1);
		collection.add(dvd2);
		collection.add(dvd3);
		
		for (Media m: collection)
		{
			System.out.println("Playing DVD: " + m.getTitle());
			System.out.println("DVD length: " + ((Disc) m).getLength());
		}
		
		//java.util.Iterator iterator = collection.iterator();
		System.out.println("________________________________________________");
		System.out.println("The DVD currently in the order are: ");
		for (Media m: collection)
		{
			System.out.println(m.getTitle());
		}
		
		java.util.Collections.sort((java.util.List) collection);
		
		System.out.println("________________________________________________");
		System.out.println("The DVD in sort order are: ");
		for (Media m: collection)
		{
			System.out.println(m.getTitle());
		}
		
		
	}
}
