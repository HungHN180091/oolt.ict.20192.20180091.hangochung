
public class Order {
	public static final int MAX_NUMBER_ORDERED = 10;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];
	
	private int qtyOrdered;

	private String dateOrder;

	public static final int MAX_LIMMITED_ORDER = 5;
	private static int nbOrders = 0;

	public String getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(String dateOrder) {
		this.dateOrder = dateOrder;
	}

	public Order(String date)
	{
		if (nbOrders >= MAX_LIMMITED_ORDER)
			throw new ExceptionInInitializerError("The number of orders is maximum! You cannot add more.");
		this.dateOrder = date;
		nbOrders++;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}

	public void addDigitalVideoDisc(DigitalVideoDisc disc)
	{
		if (qtyOrdered < MAX_NUMBER_ORDERED) {
			this.itemsOrdered[qtyOrdered++] = disc;
			System.out.println("Disc has been added!");
		}
		else System.out.println("The order queue is full!");
	}

	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList)
	{
		if (qtyOrdered + dvdList.length > MAX_NUMBER_ORDERED)
		{
			if (qtyOrdered < MAX_NUMBER_ORDERED)
				System.out.println("The add list is too long!");
			else System.out.println("The order queue is full!");
		}
		for (DigitalVideoDisc digitalVideoDisc : dvdList) {
			addDigitalVideoDisc(digitalVideoDisc);
		}
	}

	/*
	public void addDigitalVideoDisc(DigitalVideoDisc ... disc)
	{
		if (qtyOrdered + disc.length > MAX_NUMBER_ORDERED)
		{
			if (qtyOrdered < MAX_NUMBER_ORDERED)
				System.out.println("The add list is too long!");
			else System.out.println("The order queue is full!");
		}
		for (DigitalVideoDisc digitalVideoDisc : disc) {
			addDigitalVideoDisc(disc);
		}
	}  DUPLICATE WITH THE METHOD WITH ARRAY ARGUMENT
	*/

	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2)
	{
		if (qtyOrdered < MAX_NUMBER_ORDERED - 2)
		{
			addDigitalVideoDisc(dvd1);
			addDigitalVideoDisc(dvd2);
		}
		else System.out.println("The dvd queue contain less than 2 slots.");
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc)
	{
		for (int i=0;i < qtyOrdered;i++)
		{
			if (itemsOrdered[i] == disc)
			{
				for (int j=i;j< qtyOrdered-1; j++ )
					itemsOrdered[j] = itemsOrdered[j+1];
				itemsOrdered[qtyOrdered-1] = null;
				qtyOrdered--;
				System.out.println("Disc #" + (i+1) + " has been removed successfully!");
				return;
			}
		}
	}
	
	public float totalCost()
	{
		float cost = 0;
		for (int i=0; i<qtyOrdered; i++)
		{
			cost += itemsOrdered[i].getCost();
		}
		return  cost;
	}

	public void displayOrder()
	{
		System.out.println("************************** Order **************************");
		System.out.println("Date: " + dateOrder);
		System.out.println("Order items: ");
		for (int i = 0; i < qtyOrdered; i++)
			System.out.println((i+1) + ". DVD - " + itemsOrdered[i].getTitle() + " - "
								+ itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getLength()
								+ ": " + itemsOrdered[i].getCost());
		System.out.println("Total cost: " + totalCost());
		System.out.println("***********************************************************");
	}
}
